-- The MIT License
--
-- Copyright 2014 Ned Hyett.
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
if(CLIENT) then
	function tableLen( tab )
		local count = 0
		for _ in pairs( tab ) do count = count + 1 end
		return count
	end
	
	CreateClientConVar("ChatBubbles_Enabled", 1, true, false)
	CreateClientConVar("ChatBubbles_Timeout", 8, true, false)
	CreateClientConVar("ChatBubbles_TextColour_R", 0, true, false)
	CreateClientConVar("ChatBubbles_TextColour_G", 0, true, false)
	CreateClientConVar("ChatBubbles_TextColour_B", 0, true, false)
	CreateClientConVar("ChatBubbles_TextColour_A", 255, true, false)
	CreateClientConVar("ChatBubbles_BackgroundColour_R", 255, true, false)
	CreateClientConVar("ChatBubbles_BackgroundColour_G", 0, true, false)
	CreateClientConVar("ChatBubbles_BackgroundColour_B", 0, true, false)
	CreateClientConVar("ChatBubbles_BackgroundColour_A", 255, true, false)

	hook.Add( "PopulateToolMenu", "ChatBubbles-PopulateToolMenu", function()
		spawnmenu.AddToolMenuOption( "Options", "ChatBubbles", "ChatBubbles_Options", "ChatBubbles", "", "", function( panel )
			panel:ClearControls()
			
			local enabledCheckbox = vgui.Create( "DCheckBoxLabel" )
			enabledCheckbox:SetText("Enabled")
			enabledCheckbox:SetConVar("ChatBubbles_Enabled")
			enabledCheckbox:SetValue( 1 )
			enabledCheckbox:SizeToContents()
			panel:AddItem(enabledCheckbox)
			
			local timeout = vgui.Create("DNumSlider")
			timeout:SetText("Bubble Timeout")
			timeout:SetMin(3)
			timeout:SetMax(15)
			timeout:SetDecimals(0)
			timeout:SetConVar( "ChatBubbles_Timeout" )
			panel:AddItem(timeout)
			
			local backgroundColourMixer = vgui.Create("DColorMixer")
			backgroundColourMixer:SetPalette(true)
			backgroundColourMixer:SetAlphaBar(true)
			backgroundColourMixer:SetWangs(true)
			backgroundColourMixer:SetColor( Color(GetConVarNumber("ChatBubbles_BackgroundColour_R"), GetConVarNumber("ChatBubbles_BackgroundColour_G"), GetConVarNumber("ChatBubbles_BackgroundColour_B"), GetConVarNumber("ChatBubbles_BackgroundColour_A")))
			
			backgroundColourMixer.ValueChanged = function(self, colour)
				RunConsoleCommand("ChatBubbles_BackgroundColour_R", colour.r)
				RunConsoleCommand("ChatBubbles_BackgroundColour_G", colour.g)
				RunConsoleCommand("ChatBubbles_BackgroundColour_B", colour.b)
				RunConsoleCommand("ChatBubbles_BackgroundColour_A", colour.a)
			end
			panel:AddItem(backgroundColourMixer)
			
			local textColourMixer = vgui.Create("DColorMixer")
			textColourMixer:SetPalette(true)
			textColourMixer:SetAlphaBar(true)
			textColourMixer:SetWangs(true)
			textColourMixer:SetColor( Color(GetConVarNumber("ChatBubbles_TextColour_R"), GetConVarNumber("ChatBubbles_TextColour_G"), GetConVarNumber("ChatBubbles_TextColour_B"), GetConVarNumber("ChatBubbles_TextColour_A")))
			
			textColourMixer.ValueChanged = function(self, colour)
				RunConsoleCommand("ChatBubbles_TextColour_R", colour.r)
				RunConsoleCommand("ChatBubbles_TextColour_G", colour.g)
				RunConsoleCommand("ChatBubbles_TextColour_B", colour.b)
				RunConsoleCommand("ChatBubbles_TextColour_A", colour.a)
			end
			panel:AddItem(textColourMixer)
		end)
	end)
	
	-- { entid, bubble }
	-- bubble = { text, time left }
	local bubbles = {}
	
	surface.CreateFont( "ChatBubblesMedium", 
		{ font="Arial", size=20, weight=1000, blursize=0, scanlines=0, antialias=true, 
			underline=false, italic=false, strikeout=false, symbol=false, rotary=false, 
			shadow=false, additive=false, outline=false
		}
	)
	hook.Add("OnPlayerChat", "ChatBubbles-OnPlayerChat", function(ply, text, team, dead)
		if(GetConVarNumber("ChatBubbles_Enabled") == 1) then
			createNewBubble(text, ply)
		end
	end)
	
	hook.Add("PostDrawOpaqueRenderables", "ChatBubbles-Draw", function()
		if(GetConVarNumber("ChatBubbles_Enabled") == 0) then
			return
		end
		for entid,localbubbles in pairs(bubbles) do
			local totalSize = tableLen(localbubbles) * 10
			for i,bubble in pairs(localbubbles) do
				drawBubble(bubble[1], ents.GetByIndex(entid), totalSize)
				totalSize = totalSize - 10
				if(bubble[2] < CurTime()) then
					table.remove(localbubbles, i)
				end
			end
		end
	end)

	function createNewBubble(text, player)
		local val = player:EntIndex()
		if(not bubbles[val]) then 
			bubbles[val] = {}
		end
		table.insert( bubbles[val], { text, CurTime() + 10 })
	end

	function drawBubble(text, player, bubblepos)
		local minBound, maxBound = player:GetModelBounds()
		local height = maxBound.z - minBound.z
		local viewAngle = Angle( 0, LocalPlayer():EyeAngles().y - 90, 90 )
		local scale = 0.25
		local pos = -100
		surface.SetFont("ChatBubblesMedium")
		local drawWidth, drawHeight = surface.GetTextSize( text )
		local bgWidth = drawWidth
		local xpos = -(bgWidth / 2)
		pos = pos + 21
		local bgHeight = pos
		surface.SetFont("Default")
		local worldPos = player:GetPos() + Vector( 0, 0, (height + bubblepos) - 20)
		
		cam.Start3D2D( worldPos, viewAngle, scale )
		draw.RoundedBox( 16, xpos - 10, -115, bgWidth + 20, bgHeight + 110, Color(GetConVarNumber("ChatBubbles_BackgroundColour_R"), GetConVarNumber("ChatBubbles_BackgroundColour_G"), GetConVarNumber("ChatBubbles_BackgroundColour_B"), GetConVarNumber("ChatBubbles_BackgroundColour_A")) )
		pos = -100
		chatbubbles_drawText(text, xpos, pos)
		cam.End3D2D()
		pos = 50
	end
	
	function chatbubbles_drawText(text, posx, posy)
		return draw.SimpleText(text, "ChatBubblesMedium", posx, posy, Color(GetConVarNumber("ChatBubbles_TextColour_R"), GetConVarNumber("ChatBubbles_TextColour_G"), GetConVarNumber("ChatBubbles_TextColour_B"), GetConVarNumber("ChatBubbles_TextColour_A")), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
	end
	
	print("ChatBubbles - loaded")
end
